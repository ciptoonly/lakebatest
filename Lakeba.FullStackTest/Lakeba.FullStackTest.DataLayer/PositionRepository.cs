﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lakeba.FullStackTest.DataLayer
{
    public class PositionRepository : IPositionRepository
    {
        public int Create(Position model)
        {
            using (var context = new LakebaContext())
            {
                context.Positions.Add(model);
                context.SaveChanges();
                return model.ID;
            }
        }

        public void Update(Position model)
        {
            using (var context = new LakebaContext())
            {
              
                    context.Positions.Attach(model);
                    var entry = context.Entry(model);
                    entry.Property(e => e.ID).IsModified = true;
                    entry.Property(e => e.Name).IsModified = true;
                      context.SaveChanges();
               
              
            }
        }

        public List<Position> GetAll()
        {
            using (var context = new LakebaContext())
            {
                return context.Positions.ToList();
            }
        }
        public int GetTotalCount(int id)
        {
            using (var context = new LakebaContext())
            {
                if (id > 0)
                    return context.Positions.Where(c => c.ID == id).Count();
                else
                    return context.Positions.Count();
            }
        }
        public int GetTotalCount()
        {
            using (var context = new LakebaContext())
            {
                return context.Positions.Count();
            }
        }
        public List<Position> Get(int startrowindex, int maximumrows, int id)
        {
            using (var context = new LakebaContext())
            {
                if (id == 0)
                    return context.Positions.OrderBy(c => c.ID).Skip(startrowindex * maximumrows).Take(maximumrows).ToList();
                else
                    return new List<Position>() { context.Positions.FirstOrDefault(c => c.ID == id) };
            }
        }
        public Position GetbyID(int id)
        {
            using (var context = new LakebaContext())
            {
                return context.Positions.FirstOrDefault(c => c.ID == id);
            }
        }


        public bool Delete(int id)
        {
            bool result = true;
            using (var context = new LakebaContext())
            {
                var deletetarget = context.Positions.FirstOrDefault(c => c.ID == id);
                if (deletetarget != null)
                {
                    context.Positions.Remove(deletetarget);
                    context.SaveChanges();
                }
                else result = false;
            }
            return result;
        }
    }
}
