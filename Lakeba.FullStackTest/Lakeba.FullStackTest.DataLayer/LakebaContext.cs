﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.Entity;

namespace Lakeba.FullStackTest.DataLayer
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class LakebaContext:DbContext
    {
        public LakebaContext()
            : base("name=MyContext") 
        {
        }
        public DbSet<Candidate> Candidates{ get; set; }
        public DbSet<Position> Positions{ get; set; }
    }
}
