﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lakeba.FullStackTest.DataLayer
{
    public class CandidateRepository:ICandidateRepository
    {
        public int Create(Candidate model)
        {
            using (var context = new LakebaContext())
            {
                context.Candidates.Add(model);
                context.SaveChanges();
                return model.ID;
            }
        }

        public void Update(Candidate model)
        {
            using (var context = new LakebaContext())
            {
              
                    context.Candidates.Attach(model);
                    var entry = context.Entry(model);
                    entry.Property(e => e.ID).IsModified = true;
                    entry.Property(e => e.Name).IsModified = true;
                    entry.Property(e => e.SurName).IsModified = true;
                    entry.Property(e => e.PositionID).IsModified = true;
                    entry.Property(e => e.CVFileName).IsModified = true;
                    context.SaveChanges();
                    
                
             
            }
        }

        public List<Candidate> GetAll()
        {
            using (var context = new LakebaContext())
            {
                return context.Candidates.Include("Position").ToList();
            }
        }
        public int GetTotalCount(int id)
        {
            using (var context = new LakebaContext())
            {
                if (id > 0)
                    return context.Candidates.Where(c => c.ID == id).Count();
                else
                    return context.Candidates.Count();
            }
        }
        public int GetTotalCount()
        {
            using (var context = new LakebaContext())
            {
                return context.Candidates.Count();
            }
        }
        public List<Candidate> Get(int startrowindex, int maximumrows, int id)
        {
            using (var context = new LakebaContext())
            {
                if (id == 0)
                    return context.Candidates.OrderBy(c => c.ID).Skip(startrowindex * maximumrows).Take(maximumrows).ToList();
                else
                    return new List<Candidate>() { context.Candidates.FirstOrDefault(c => c.ID == id) };
            }
        }
        public Candidate GetbyID(int id)
        {
            using (var context = new LakebaContext())
            {
                return context.Candidates.Include("Position").FirstOrDefault(c => c.ID == id);
            }
        }


        public bool Delete(int id)
        {
            bool result = true;
            using (var context = new LakebaContext())
            {
                var deletetarget = context.Candidates.FirstOrDefault(c => c.ID == id);
                if (deletetarget != null)
                {
                    context.Candidates.Remove(deletetarget);
                    context.SaveChanges();
                }
                else result = false;
            }
            return result;
        }
    }
}
