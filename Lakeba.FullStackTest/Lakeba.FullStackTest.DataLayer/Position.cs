﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace Lakeba.FullStackTest.DataLayer
{
    public class Position
    {
        public int ID { get; set; }
        [Required]
        public string Name { get; set; }
    }
}
