namespace Lakeba.FullStackTest.DataLayer.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Lakeba.FullStackTest.DataLayer.LakebaContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Lakeba.FullStackTest.DataLayer.LakebaContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            context.Positions.Add(new Position() {Name = "Front End Developer"});
            context.Positions.Add(new Position() { Name = "Back End Developer" });
            context.Positions.Add(new Position() { Name = "Full Stack Developer" });
            context.Positions.Add(new Position() { Name = "Team Leader" });

        }
    }
}
