﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lakeba.FullStackTest.DataLayer
{
    public interface IRepositoryBase<T>
    {
        int Create(T model);
        void Update(T model);
        List<T> GetAll();
        T GetbyID(int id);
        bool Delete(int id);
        List<T> Get(int startrowindex, int maximumrows,int id);
        int GetTotalCount();
        int GetTotalCount(int id);
    }
}
