﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lakeba.FullStackTest.DataLayer
{
    public interface ICandidateRepository:IRepositoryBase<Candidate>
    {
    }
}
