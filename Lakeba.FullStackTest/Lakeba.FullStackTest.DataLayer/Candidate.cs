﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lakeba.FullStackTest.DataLayer
{
    public class Candidate
    {
        public int ID { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string SurName { get; set; }

        public int PositionID { get; set; }
        public Position Position { get; set; }
        public string CVFileName { get; set; }

        [NotMapped]
        public string PositionName
        {
            get
            {
                return Position != null ? Position.Name:"";
            }
        }
    }
}
