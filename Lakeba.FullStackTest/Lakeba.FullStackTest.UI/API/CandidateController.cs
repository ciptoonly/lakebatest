﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Lakeba.FullStackTest.DataLayer;
using System.Web;

namespace App.Lakeba.FullStackTest.API
{
   // [Route("api/candidate")]
    public class CandidateController : ApiController
    {
        private readonly ICandidateRepository _candidateRepository=new CandidateRepository();

        [HttpGet]
        public IEnumerable GetAll()
        {
            return _candidateRepository.GetAll();
        }
        [HttpPost]
        public IHttpActionResult Create([FromBody]Candidate item)
        {
            if (item == null)
            {
                return BadRequest();
            }
            if (ModelState.IsValid)
            {
                _candidateRepository.Create(item);
                return Created(new Uri(Request.RequestUri + item.ID.ToString()), item);
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpGet]
        [Route("api/candidate/getbyid/{id}")]
        public IHttpActionResult GetById(int id)
        {
            var item = _candidateRepository.GetbyID(id);
            if (item == null)
            {
                return NotFound();
            }
            return Ok(item);
        }
        [HttpPut]
        public IHttpActionResult Update(string id, [FromBody] Candidate item)
        {
            if (ModelState.IsValid)
            {
                _candidateRepository.Update(item);
                return Ok();
            }
            else
            {
                return BadRequest();
            }
           
        }
        [HttpDelete]
 
        public bool DeleteProduct(int id)
        {
            if (_candidateRepository.Delete(id))
            {
                return true;
            }
            return false;
        }
        [HttpPost]
        [Route("candidate/uploadcv")]
        public IHttpActionResult Post()
        {
            var httpRequest = HttpContext.Current.Request;
            if (httpRequest.Files.Count < 1)
            {
                return BadRequest();
            }

            foreach (string file in httpRequest.Files)
            {
                var postedFile = httpRequest.Files[file];
                var filePath = HttpContext.Current.Server.MapPath("~/Content/uploaded/" + postedFile.FileName);
                postedFile.SaveAs(filePath);
            }

            return Ok();
        }
    }
}
