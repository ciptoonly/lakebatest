﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Lakeba.FullStackTest.DataLayer;

namespace App.Lakeba.FullStackTest.API
{
    public class PositionController : ApiController
    {
        private readonly IPositionRepository _positionRepository = new PositionRepository();
        [HttpGet]
        public IEnumerable GetAll()
        {
            return _positionRepository.GetAll();
        }
    }
}
