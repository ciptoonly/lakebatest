// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RouteConfig.cs" company="">
//   Copyright � 2016 
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace App.Lakeba.FullStackTest
{
    using System.Web.Routing;

    using App.Lakeba.FullStackTest.Routing;

    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.Add("Default", new DefaultRoute());
        }
    }
}
