// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BundleConfig.cs" company="">
//   Copyright � 2016 
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace App.Lakeba.FullStackTest
{
    using System.Web;
    using System.Web.Optimization;

    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/content/css/app")
                .Include("~/content/app.css")
                .Include("~/content/ui-grid.min.css")
                .Include("~/content/loading-bar.min.css")
                
                );

          
            bundles.Add(new ScriptBundle("~/js/app").Include(
                 "~/scripts/vendor/ng-file-upload.min.js",
               "~/scripts/vendor/ng-file-upload-shim.min.js",
                "~/scripts/vendor/angular-ui-router.js",
                 "~/scripts/vendor/loading-bar.min.js",
                "~/scripts/filters.js",
                "~/scripts/services.js",
                "~/scripts/directives.js",
                "~/scripts/controllers.js",
                "~/scripts/app.js"));
        }
    }
}
