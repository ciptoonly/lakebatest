﻿'use strict';

// Demonstrate how to register services
// In this case it is a simple value service.
var app = angular.module('app.services', ['angular-loading-bar']);
app.value('version', '0.1');

app.factory('candidateservices', [
    '$http', function(http) {
        return {
            getByID:function(id) {
                return http.get('/api/candidate/getbyid/'+id);
            },
            create:function(candidate) {
                return http.post('/api/candidate', candidate);
            },
            getAll:function() {
                return http.get('/api/candidate');
            },
            Delete:function(id) {
                return http.delete('/api/candidate/'+id);
            },
            update:function(id,candidate) {
                return http.put('/api/candidate/' + id, candidate);
            }
        }
    }]);
app.factory('positionservices', [
    '$http', function (http) {
        return {
            getAll: function () {
                return http.get('/api/position');
            }
        }
    }]);