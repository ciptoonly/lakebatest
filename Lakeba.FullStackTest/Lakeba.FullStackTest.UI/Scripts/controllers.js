﻿'use strict';

// Google Analytics Collection APIs Reference:
// https://developers.google.com/analytics/devguides/collection/analyticsjs/

angular.module('app.controllers', ['ui.bootstrap','ngFileUpload'])

    // Path: /
    .controller('HomeCtrl', [
        '$scope', '$location', '$window', function($scope, $location, $window) {
            $scope.$root.title = 'Lakeba Test';
            $scope.$on('$viewContentLoaded', function() {
                //send analytics

            });
        }
    ])

    // Path: /candidate
    .controller('CandidateCtrl', [
        '$scope', '$location', '$window', '$uibModal', 'candidateservices', 'positionservices', 'Upload', function ($scope, $location, $window, $uibModal, candidateservices, positionservices, Upload) {
            $scope.$root.title = 'Lakeba Test | Candidate';
            $scope.mode = 'List';
            $scope.candidates = [];
            $scope.positions = [];
            $scope.gridOptions = {
                enableSorting: true,
                columnDefs: [
                   
                    { name: 'Name', field: 'Name' },
                    { name: 'Surname', field: 'SurName' },
                    { name: 'Position', field: 'Position.Name' },
                    { name: 'Curiculum', field: 'CVFileName', cellTemplate: '<div class="ui-grid-cell-contents" ><a class="pointer" target="_blank" download href="/Content/Uploaded/{{grid.getCellValue(row, col)}}">{{grid.getCellValue(row, col)}}</a></div>' },
                    { name: 'Action Requested', cellTemplate: '<div class="ui-grid-cell-contents" ><a class="pointer" href ng-click="grid.appScope.Edit(row.entity.ID)" class="pointer">Edit</a>&nbsp;<a href ng-click="grid.appScope.Delete(row.entity.ID)" class="pointer">Delete</a></div>' }

                    
                ]
                ,data:[]
            };
            $scope.candidate= {
                Name: '',
                SurName: '',
                PositionID: 0,
                CVFileName: '',
                TemporaryFile: null,
                PositionName:''
            }
            $scope.emptycandidate = angular.copy($scope.candidate);
            $scope.Add = function () {
                $scope.candidate = angular.copy($scope.emptycandidate);
                $scope.mode = 'Add';
                $scope.OpenPopUp();
                
            };
            $scope.Edit = function (id) {
                $scope.mode = 'Edit';
                candidateservices.getByID(id).success(function (data) {
                    $scope.candidate.ID = data.ID;
                    $scope.candidate.Name = data.Name;
                    $scope.candidate.SurName = data.SurName;
                    $scope.candidate.CVFileName = data.CVFileName;
                    $scope.candidate.PositionID = data.PositionID;
                    $scope.candidate.PositionName = data.Position.Name;
                    $scope.OpenPopUp();
                });
            };
            $scope.IstheSamePositionID=function(positionid) {
                return $scope.candidate.PositionID == positionid;
            }
            $scope.modalInstance = null;
            $scope.OpenPopUp = function () {
                positionservices.getAll().success(function (data) {
                    $scope.positions = data;
                    $scope.modalInstance = $uibModal.open({
                        templateUrl: 'myModalContent.html',
                        bindToController: true,
                        scope: $scope
                    });

                    $scope.modalInstance.result.then(function (selectedItem) {

                    }, function () {

                    });
                });
                
            }
            $scope.setPosition = function (id,positionname) {
                $scope.candidate.PositionID = parseInt(id);
                $scope.candidate.PositionName = positionname;
            }
            $scope.uploadCV = function (file) {
                file.upload = Upload.upload({
                    url: '/candidate/uploadcv',
                    data: { username: $scope.username, file: file }
                });
                
                file.upload.then(function (response) {
                    //if (response.status == 200) {
                       
                    //}
                  
                }, function (response) {
                    if (response.status > 0)
                        $scope.alerts.push({ msg: 'Error Uploading File: '+response.status + ': ' + response.data, type: 'danger' });
                }, function (evt) {
                    // Math.min is to fix IE which reports 200% sometimes
                    file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                });
            }
            $scope.ClosePopUp=function() {
                $scope.modalInstance.close();
            }
            $scope.Delete = function (id) {
                candidateservices.Delete(id).success(function (data) {
                    $scope.alerts.push({ msg: 'Candidate Deleted!', type: 'success' });
                    $scope.rebindGrid();
                });
            }
            $scope.Save = function () {
               
                if ($scope.candidate.TemporaryFile != null) {
                    $scope.candidate.CVFileName =$scope.candidate.TemporaryFile.name;
                    $scope.uploadCV($scope.candidate.TemporaryFile);
                }
                
               if ($scope.mode == 'Edit') {
                   candidateservices.update($scope.candidate.ID, $scope.candidate).success(function (data) {
                       $scope.ClosePopUp();
                       $scope.alerts.push({ msg: 'Candidate Updated!', type: 'success' });
                       $scope.rebindGrid();
                   });
               } else {
                   //Add
                   candidateservices.create($scope.candidate).success(function (data) {
                       $scope.ClosePopUp();
                       $scope.alerts.push({ msg: 'Candidate Created!', type: 'success' });
                       $scope.rebindGrid();
                   });
               }
               Quixxi.q.push(['SendEvent', {
                   "event_category": $scope.mode,
                   "event_value": $scope.mode,
                   "event_detail": $scope.candidate.Name,
                   "event_object": $scope.candidate
               }]);
            }
            $scope.rebindGrid=function()
            {
                candidateservices.getAll().success(function (data) {
                    $scope.candidates = [];
                    $scope.gridOptions.data = [];
                    for (var i = 0; i < data.length; i++) {
                        $scope.candidates.push(data[i]);
                        $scope.gridOptions.data.push(data[i]);
                    }

                });
            }
            $scope.rebindGrid();
            $scope.closeAlert = function (index) {
                $scope.alerts.splice(index, 1);
            };

            $scope.alerts = [];
            $scope.$on('$viewContentLoaded', function () {
                //send analytics

                //bind grid to list
               
               
               

            });
        }
    ]);

   

   